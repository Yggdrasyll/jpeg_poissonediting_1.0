#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 18:34:58 2018

@author: Ismael Chouraqi
"""

import scipy.signal as sp 
import numpy as np
import cv2
import sys
import time
import argparse
import os

def createFolder(Path):
    try:
        if not os.path.exists(Path):
            os.makedirs(Path)
    except OSError:
        print ('Error: Creating directory. ' + Path)

def interpolation(u, v, mask, iteation = 100):
    t = time.time()
    K = np.array([[0, 1, 0],[1, 0, 1],[0, 1, 0]])*1.
    Lapv = cv2.Laplacian(v, cv2.CV_64F)
    aMask = 1 - mask
    r = v.copy()
    for i in range(iteation):
        s = i  % 25
        print("{} % Processing interpolation {} *".format(np.round(i * 100./iteation, 1), ' ' * s))    
        sys.stdout.write("\033[F")
        r = np.stack([sp.convolve2d(r[:, :, c] * mask[:, :, c] + u[:, :, c] * aMask[:, :, c], K, 'same') for c in range(3)], 2)
        r = 0.25*(r - Lapv)
    t2 = time.time()
    print("Process time : {} s".format(t2 - t))
    return r

def DimensionedMask(Mask, dimension, position):
    if dimension[0] < Mask.shape[0] or dimension[1] < Mask.shape[1]:
        raise ValueError("you can't shrink the dimensions of your mask")
    if position[0] < 0 or position[1] < 0 or position[0] + Mask.shape[0] > dimension[0] or position[1] + Mask.shape[1] > dimension[1] :
        raise ValueError("you can't set your mask outside new bounderies")
    nMask = np.zeros(dimension)
    nMask[position[0] : position[0] + Mask.shape[0], position[1] : position[1] + Mask.shape[1]] = Mask
    return nMask

def LocalBackGround(FBG, dimension, position):
    if dimension[0] > FBG.shape[0] or dimension[1] > FBG.shape[1]:
        raise ValueError("you can't extend the dimensions of your background")
    if position[0] < 0 or position[1] < 0 or position[0] + dimension[0] > FBG.shape[0] or position[1] + dimension[1] > FBG.shape[1] :
        raise ValueError("you can't set your local background outside of bounderies")
    mask = np.zeros(FBG.shape)
    mask[position[0] + 2 : position[0] + dimension[0] - 2, position[1] + 2 : position[1] + dimension[1] - 2] = 1.
    return FBG[position[0] : position[0] + dimension[0], position[1] : position[1] + dimension[1]], mask

def JPEG_Mask(u):
    mask = np.ones([8, 8])
    mask[1:8, 1:8] = 0
    mask = np.array(list(np.array((u.shape[1]//8) * list(mask)).T) * (u.shape[0]//8))
    mask = np.stack([mask]*3, 2)
    return mask

def PSNR(VT, Op):
        try :
            EQM = np.sqrt(np.mean((VT - Op)**2))
        except ValueError:
            Op = Op[:,:,0]
            EQM = np.sqrt(np.mean((VT - Op)**2))
        psnr = 20*np.log10(255*1./EQM)
        return psnr

if __name__ == '__main__':
    #Création du parser
    JPEParser = argparse.ArgumentParser()
     
    #Ajout D'une image
    JPEParser.add_argument("-I", help="Single image compressed with lossy DCT based JPEG")
    
    #Ajout D'une image
    JPEParser.add_argument("-o", help="Name of the Output with format")
    
    #Groundtruth (optionnal)
    JPEParser.add_argument("-gt", help="Single ground truth image (optional)")
    
    #On parse les arguments
    args = JPEParser.parse_args()
    
    if not args.I:
        raise AttributeError("""You must give an input image as follow:
            JPEG_PoissonEditing.py -I Input.format""")
        
    if not args.o:
        raise AttributeError("""You must give an output name as follow:
            JPEG_PoissonEditing.py -o Output.format""")
    
    Path = args.I
    Image = cv2.imread(Path, cv2.CV_64F)*1/255.
    try :
        if Image.shape[2] == 1 :
            Image = Image.reshape([Image.shape[0], Image.shape[1]])
            Image = np.stack([Image] * 3, 2)
    except IndexError:
        Image = np.stack([Image] * 3, 2)
        
    mask = JPEG_Mask(Image)
    Img_loc, nMask = LocalBackGround(Image, mask.shape, [0, 0])
    r = interpolation(Img_loc, np.zeros(mask.shape), mask)
    nr = (1 - nMask) * Image + DimensionedMask(r, Image.shape, [0, 0]) * nMask
    createFolder('Results/')
    cv2.imwrite('Results/{}'.format(args.o), nr*255)
    
    if args.gt:
        GroundTruth = cv2.imread(args.gt, cv2.CV_64F)*1/255.
        print("PSNR JPEG : {}".format(PSNR(GroundTruth, Image)))
        print("PSNR Poisson restored : {}".format(PSNR(GroundTruth, nr)))