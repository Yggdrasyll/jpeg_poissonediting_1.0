# JPEG Poisson Editing in Python

I made this script for personal use based on a simple and quick idea. If you use this code or if this code was useful to you, do not forget to mention it in your contributions.

## Getting Started


### Prerequisites

* Numpy
* scipy
* Os
* OpenCv
* sys

For Windows users, you will need to change '/' by '\\'.

### Running the tests

Run the JPEG_PoissonEditing.py script in a terminal
```
python JPEG_PoissonEditing.py -I compressed_input.format -o output_name.format -gt groundtruth.format(optional)
```

## Built With

* [Python](https://www.python.org/downloads/) 
* [OpenCV](https://github.com/opencv/opencv)


## Authors

* **Ismaël Chouraqi** - *Ph.D at université Paris Descartes - MAP5* - [Gitlab](https://gitlab.com/Yggdrasyll)


## Acknowledgments

* Patrick Pérez, Michel Gangnet, Andrew Blake [PDF](https://www.cs.virginia.edu/~connelly/class/2014/comp_photo/proj2/poisson.pdf)


